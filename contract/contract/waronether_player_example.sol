contract PlayerInterface{function execute(WarOnEther war);}

contract WarOnEther{function WarOnEther(uint256 numRoundsToSet);function changePhase(uint targetPhase);function move(uint256 x,uint256 y);function round()constant returns(uint256 );function registrationFee()constant returns(uint256 );function getBoardOwners()constant returns(address[16][16] );function currentPlayer()constant returns(uint256 );function claimTile(uint256 x,uint256 y);function commit(bytes32 contractHash,address rewardAddress);function boardSize(uint256 )constant returns(uint8 );function executeNext();function board(uint256 ,uint256 )constant returns(address owner,bool isLife);function start(uint256 seed);function getRegisteredContracts()constant returns(PlayerInterface[5] );function playerTiles(address )constant returns(uint256 );function numPlayers()constant returns(uint256 );function numRounds()constant returns(uint256 );function register(address contractAddress,address rewardAddress);function phase()constant returns(uint );function committedContracts(uint256 )constant returns(bytes32 );function playerLifeTiles(address )constant returns(uint256 );function finish();function getBoardLifeTiles()constant returns(address[16][16] );function emergencyWithdraw();function registeredContracts(uint256 )constant returns(PlayerInterface );}

contract WarOnEther_Player_Example is PlayerInterface {

  uint _x;
  uint _y;

  function WarOnEther_Player_Example(uint startX, uint startY) {
    _x = startX;
    _y = startY;
  }
  function execute(WarOnEther war) {
    if (_x == 2) war.move(2, 8);
    else war.claimTile(_x, _y);
    if (_x == (war.boardSize(0) - 1)) {
      _x = 0;
      if (_y == (war.boardSize(1) - 1)) {
        _y = 0;
      } else {
        _y++;
      }
    } else {
      _x++;
    }
  }
}
