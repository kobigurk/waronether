var assert = require('assert');
var Workbench = require('ethereum-sandbox-workbench');

var workbench = new Workbench();

workbench.startTesting(['waronether', 'waronether_player_example'], function(contracts) {
  var numPlayers = 5;
  var player1;
  var player2;
  var war;

  var rewardAddresses = {};

  it('deploys game', function() {
    return contracts.WarOnEther_Player_Example.new(1,1)
    .then(function (contract) {
      player1 = contract;
      rewardAddresses[player1.address] = '0x1111111111111111111111111111111111111111';
      return contracts.WarOnEther_Player_Example.new(14,10)
    })
    .then(function (contract) {
      player2 = contract;
      rewardAddresses[player2.address] = '0x2222222222222222222222222222222222222222';
      return contracts.WarOnEther.new(2);
    })
    .then(function(contract) {
      if (contract.address) {
        war = contract;
        return true;
      }
      else throw new Error('No address for contract');
    });
  });

  it('registers', function() {
    var player1Hash = workbench.sandbox.web3.sha3(workbench.sandbox.web3.eth.getCode(player1.address), {encoding: 'hex'});
    var player2Hash = workbench.sandbox.web3.sha3(workbench.sandbox.web3.eth.getCode(player2.address), {encoding: 'hex'});
    return war.register(player1.address, rewardAddresses[player1.address])
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      assert.equal(war.registeredContracts(0), '0x0000000000000000000000000000000000000000');
      return war.commit('0x' + player1Hash, rewardAddresses[player1.address], {
        value: workbench.sandbox.web3.toWei('1')
      });
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function() {
      return war.commit('0x' + player2Hash, rewardAddresses[player2.address], {
        value: workbench.sandbox.web3.toWei('1')
      });
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function() {
      return war.changePhase(1);
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      return war.register(player1.address, rewardAddresses[player1.address], {
        gas: 3000000
      });
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function() {
      return war.register(player2.address, rewardAddresses[player2.address], {
        gas: 3000000
      });
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      var registerLog = receipt.logs[0];
      assert.equal(registerLog.parsed.event, 'ContractRegistered');
      assert.equal(war.registeredContracts(0), player1.address);
      assert.equal(war.registeredContracts(1), player2.address);
      for (var i = 2; i < numPlayers; i++) {
        assert.equal(war.registeredContracts(i), '0x0000000000000000000000000000000000000000');
      }
      return war.changePhase(2);
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      return true;
    });
  });

  function printBoard() {
    console.log();
    var board = war.getBoardOwners();
    var boardLife = war.getBoardLifeTiles();
    for (var y = 0; y < 16; y++) {
      for (var x = 0; x < 16; x++) {
        if (board[x][y] == player1.address) {
          if (boardLife[x][y] == player1.address)
            process.stdout.write('A');
          else 
            process.stdout.write('1');
        } else if (board[x][y] == player2.address) {
          if (boardLife[x][y] == player2.address)
            process.stdout.write('B');
          else 
            process.stdout.write('2');
        } else {
          process.stdout.write('*');
        }
      }
      process.stdout.write('\n');
    }
  }

  it('start the game', function() {
    return war.start(1000)
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      return true;
    });
  });

  function runRound() {
    return war.executeNext({
      gas: 3000000
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      printBoard();
      return true;
    });
  }
  it('plays all rounds', function() {
    var ret = Promise.resolve();
    for (var i = 0; i < war.numRounds()*2; i++) {
      ret = ret.then(runRound);
    }
    return ret;
  });

  it('determines winner', function() {
    var initialBalance = workbench.sandbox.web3.eth.getBalance(workbench.defaults.from);
    return war.finish({
      gas: 1000000
    })
    .then(function(txHash) {
      return workbench.waitForReceipt(txHash);
    })
    .then(function(receipt) {
      var winLog = receipt.logs[0];
      assert.equal(winLog.parsed.args.addr, player2.address);
      assert.equal(workbench.sandbox.web3.eth.getBalance(rewardAddresses[winLog.parsed.args.addr]).toString(), workbench.sandbox.web3.toWei(1.8));
      var balanceDiff = workbench.sandbox.web3.eth.getBalance(workbench.defaults.from).minus(initialBalance);
      assert(balanceDiff.greaterThanOrEqualTo(workbench.sandbox.web3.toWei(0.199)));
      assert(balanceDiff.lessThanOrEqualTo(workbench.sandbox.web3.toWei(0.201)));
      return true;
    });
  });
});
