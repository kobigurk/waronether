contract PlayerInterface {
  function execute(WarOnEther war);
}

contract WarOnEther {
  uint8 constant _numLifeTiles = 5;
  uint constant _maxNumPlayers = 5;
  uint public numPlayers = 0;

  bytes32[5] public committedContracts;
  PlayerInterface[5] public registeredContracts;
  address[5] rewardAddresses;
  mapping(address => uint) public playerTiles;
  mapping(address => uint) public playerLifeTiles;
  mapping(address => uint[2]) private playerLifeLocation;

  uint public registrationFee = 1 ether;

  event ContractCommitted(bytes32 hash);
  event ContractRegistered(address addr);
  event Claimed(address addr, uint x, uint y);
  event Lost(address addr, uint x, uint y);
  event Winner(address addr, uint256 amount);
  event Turn(address addr, bool didRun);

  uint constant _baseGasPerPlayer = 200000;
  uint constant _bonusPerTile = 10000;
  uint constant _bonusPerLifeTile = 50000;

  address _creator;

  uint8[2] public boardSize = [16, 16];
  BoardTile[16][16] public board;

  struct BoardTile {
    address owner;
    bool isLife;
  }
 
  uint public numRounds;
  uint public round = 0;
  uint public currentPlayer = 0;


  enum Phase { Commit, Register, Play }
  Phase public phase = Phase.Commit;
  function WarOnEther(uint numRoundsToSet) {
    _creator = msg.sender;
    numRounds = numRoundsToSet;
  }

  function commit(bytes32 contractHash, address rewardAddress) {
    if (phase != Phase.Commit) throw;
    if (msg.value < registrationFee) throw;
    if (numPlayers == _maxNumPlayers) throw;
    rewardAddresses[numPlayers] = rewardAddress;
    committedContracts[numPlayers] = contractHash;
    numPlayers++;
    ContractCommitted(contractHash);
  }

  function register(address contractAddress, address rewardAddress) {
    bytes32 hash;
    if (phase != Phase.Register) throw;
    assembly {
      let size := extcodesize(contractAddress)
      let o_code := mload(0x40)
      mstore(0x40, add(o_code, and(add(add(size, 0x20), 0x1f), not(0x1f))))
      mstore(o_code, size)
      extcodecopy(contractAddress, add(o_code, 0x20), 0, size)
      hash := sha3(add(o_code, 0x20), size)
    }
    bool found = false;
    uint256 foundIndex = 0;
    for (var i = 0; i < numPlayers; i++) {
      if (committedContracts[i] == hash && rewardAddresses[i] == rewardAddress) {
        found = true;
        foundIndex = i;
        break;
      }
    }
    if (!found) throw;
    registeredContracts[foundIndex] = PlayerInterface(contractAddress);
    ContractRegistered(contractAddress);
  }

  function changePhase(Phase targetPhase) onlyCreator {
    if (targetPhase <= phase) throw;
    phase = targetPhase;
  }

  function getRegisteredContracts() constant returns (PlayerInterface[5]) {
    return registeredContracts;
  }

  modifier onlyCreator() {
    if (msg.sender != _creator) throw;
    _
  }

  function start(uint256 seed) onlyCreator {
    if (phase != Phase.Play) throw;
    var currentRandom = sha3(seed);
    for (var i = 0; i < numPlayers; i++) {
      currentRandom = sha3(currentRandom);
      uint8 x = uint8(uint256(currentRandom) % boardSize[0]);
      currentRandom = sha3(currentRandom);
      uint8 y = uint8(uint256(currentRandom) % boardSize[1]);
      playerLifeLocation[registeredContracts[i]] = [x, y];
      for (var j = 0; j < _numLifeTiles; j++) {
        if (board[x][y].owner != 0) {
          throw;
        }
        board[x][y].owner = registeredContracts[i];
        board[x][y].isLife = true;
        if (x == boardSize[0] - 1) {
          x = 0;
          if (y == boardSize[1] - 1) {
            y = 0;
          } else {
            y++;
          }
        } else {
          x++;
        }
      }
      playerLifeTiles[registeredContracts[i]] = _numLifeTiles;
    }

  }

  function executeNext() onlyCreator {
    if (phase != Phase.Play) throw;
    if (round == numRounds) throw;
    var playerGas = _baseGasPerPlayer + playerTiles[registeredContracts[currentPlayer]]*_bonusPerTile + playerLifeTiles[registeredContracts[currentPlayer]]*_bonusPerLifeTile;
    var previousPlayer = currentPlayer;
    if (currentPlayer == (numPlayers - 1)) {
      round++;
      currentPlayer = 0;
    } else {
      currentPlayer++;
    }
    bool didRun = registeredContracts[previousPlayer].call.gas(playerGas)(bytes4(sha3("execute(address)")), this);
    Turn(registeredContracts[previousPlayer], didRun);
  }

  function claimTile(uint x, uint y) {
    var tile = board[x][y];
    if (tile.owner != 0) {
      if (tile.isLife) {
        playerLifeTiles[tile.owner]--;
      } else {
        playerTiles[tile.owner]--;
      }
      Lost(tile.owner, x, y);
    }

    playerTiles[msg.sender]++;
    tile.owner = msg.sender;
    tile.isLife = false;
    Claimed(msg.sender, x, y);
  }

  function move(uint x, uint y) {
    var lifeLocationX = playerLifeLocation[msg.sender][0];
    var lifeLocationY = playerLifeLocation[msg.sender][1];
    uint8 j;
    for (j = 0; j < _numLifeTiles; j++) {
      var tile = board[lifeLocationX][lifeLocationY];
      if (tile.owner == msg.sender && tile.isLife) {
        tile.owner = 0;
        tile.isLife = false;
      }
      if (lifeLocationX == boardSize[0] - 1) {
        lifeLocationX = 0;
        if (lifeLocationY == boardSize[1] - 1) {
          lifeLocationY = 0;
        } else {
          lifeLocationY++;
        }
      } else {
        lifeLocationX++;
      }
    }
    playerLifeLocation[msg.sender] = [x, y];
    for (j = 0; j < _numLifeTiles; j++) {
      var tile2 = board[x][y];
      if (tile2.owner == msg.sender) {
        if (!tile2.isLife) {
          playerTiles[msg.sender]--;
          Lost(msg.sender, x, y);
        }
      } else if (tile2.owner != 0)
        throw;
      tile2.owner = msg.sender;
      tile2.isLife = true;
      if (x == boardSize[0] - 1) {
        x = 0;
        if (y == boardSize[1] - 1) {
          y = 0;
        } else {
          y++;
        }
      } else {
        x++;
      }
    }
  }


  function getBoardOwners() constant returns(address[16][16]) {
    address[16][16] memory ret;
    for (var x = 0; x < boardSize[0]; x++)
    for (var y = 0; y < boardSize[0]; y++)
      ret[x][y] = board[x][y].owner;
    return ret;
  }
  function getBoardLifeTiles() constant returns(address[16][16]) {
    address[16][16] memory ret;
    for (var x = 0; x < boardSize[0]; x++)
    for (var y = 0; y < boardSize[0]; y++)
      if (board[x][y].isLife)
        ret[x][y] = board[x][y].owner;
    return ret;
  }

  function finish() onlyCreator {
    if (round != numRounds) throw;
    var maxIndex = 0;
    var maxTiles = playerTiles[registeredContracts[0]] + playerLifeTiles[registeredContracts[0]];
    for (var i = 1; i < numPlayers; i++) {
      var currentTiles = playerTiles[registeredContracts[i]] + playerLifeTiles[registeredContracts[i]];
      if (currentTiles > maxTiles) {
        maxIndex = i;
        maxTiles = currentTiles;
      }
    }
    var winBalance = (this.balance*9)/10;
    var leftBalance = this.balance - winBalance;
    if (!rewardAddresses[maxIndex].send(winBalance))
      throw;
    if (!_creator.send(leftBalance))
      throw;
    Winner(registeredContracts[maxIndex], winBalance);
  }

  function emergencyWithdraw() onlyCreator {
    if (!_creator.send(this.balance))
      throw;
  }

  function () {
    throw;
  }
}
