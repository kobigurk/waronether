var React = require('react');

var Board = require('./Board.jsx');

var App = React.createClass({
  getInitialState() {
    return null;
  },
  render() {
    var contractAddress = '0x80c130c2ea2f7b0292ec73f4aa4ec92d8faafe26';
    var commitText = 'Commit the hash of your contract to the game by calling the "commit" with your contract address and reward address, together with a registration fee of 1 ether. The commit phases does not reveal the contract address or its code to protect you. The easiest way to get your contract hash is by calling web3.eth.getCode(\'0x...\') on your contract address and then web3.sha3(resultFromBefore, {encoding: \'hex\'}).';
    return (
      <div>
        <div className="col-md-12">
          <h1 className="titleText">
            War On Ether
          </h1>
          <h3 className="text-center">
            Ethereum smart-contract based game
          </h3>
          <div className="col-md-6">
            <h3>
              Example game deployed right now
            </h3>
            <h5>
              (Private network @ rpc.waronether.com)
            </h5>
            <Board contractAddress={contractAddress} />
          </div>
          <div className="col-md-6">
            <h3>
              How to play
            </h3>
            <ul>
              <li>
              First, create a bot contract having an "execute" function which the game contract will run each round:
                <ul>
                  <li>
                    Your bot will be allocated gas proportional to the number of tiles you own and life tiles you have left.
                  </li>
                  <li>
                    You may use the following methods to manipluate the board within your gas limit: war.claimTile(x,y), war.move(x,y).
                  </li>
                  <li>
                    Beware - if you go over the gas limit, or cause an error, your moves in this round will be reverted!
                  </li>
                </ul>
              </li>
              <li>
              {commitText}
              </li>
              <li>
              After the game creator changes the game phase to "Register", other players can no longer commit. In this phase, you register to the game by submitting your contract address and reward address for verification.
              </li>
              <li>
              The game creator will now change the game phase to "Play" and run the next rounds until reaching the final round.
              </li>
              <li>
              The winner will be the owner of the most tiles and will be awarded the entire pool!
              </li>
            </ul>
            <div>
              <a href="/files/war_abi.json">
                Contract ABI
              </a>
              <br />
              <a href="/files/waronether_player_example.sol">
                Player contract example
              </a>
              <br />
              <a href="/files/waronether.sol">
                War On Ether contract source
              </a>

            </div>
            <h3>This is not a Battleship game!</h3>
            <h4>Key differences:</h4>
            <ul>
              <li>
                Your goal is to capture as many tiles as you can, not to sink the enemy.
              </li>
              <li>
                Your bot contract is written beforehand and then runs autonmically during the execution of the game.
              </li>
              <li>
                You get a variable amount of gas each round, depending on the number of tiles you own. You have to use it wisely to capture, move yourself, scan the board and storing state variables.
              </li>
            </ul>
            <h3>
              Future plans
            </h3>
            <ul>
              <li>
                Deploy an instance on the live network.
              </li>
              <li>
                Add helper UI on the website to ease on-boarding.
              </li>
              <li>
                Support showing multiple games on the website.
              </li>
            </ul>

          </div>
        </div>
        <div className="col-md-offset-4 col-md-4 text-center">
          <a className="btn btn-social btn-lg btn-twitter" href="https://twitter.com/war_on_ether" target="_blank">
            <i className="fa fa-twitter"></i>
            Follow us on Twitter!
          </a>
        </div>
        <div className="col-md-offset-4 col-md-4 text-center">
          If you want to hear more or run an instance of the game, contact me at <a href="mailto:kobigurk@gmail.com">kobigurk@gmail.com</a>
        </div>

      </div>
    );
  }
});

module.exports = App;
