var React = require('react');

var distinctColors = require('distinct-colors');
var async = require('async');
var Web3 = require('web3');
var web3;

var BoardAbi = require('../js/BoardAbi.js');
var Board = React.createClass({
  getInitialState() {
    return {
      contractInstance: null,
      contractInfo: null,
      rows: 0,
      cols: 0
    };
  },
  getContractInfo(contractInstance, registeredContracts, cb) {
    async.parallel([
      function(cb) {
        contractInstance.round(cb);
      },
      function(cb) {
        contractInstance.numRounds(cb);
      },
      function(cb) {
        contractInstance.currentPlayer(cb);
      },
      function(cb) {
        contractInstance.numPlayers(cb);
      },
      function(cb) {
        contractInstance.getBoardOwners(cb);
      },
      function(cb) {
        contractInstance.getBoardLifeTiles(cb);
      }
    ], function(err, results) {
      if (err) {
        console.log(err);
        return cb(err);
      }
      var contractInfo = {
        address: contractInstance.address,
        round: parseInt(results[0].toFixed(0)) + 1,
        numRounds: results[1].toFixed(0),
        currentPlayer: registeredContracts[parseInt(results[2].toFixed(0))],
        numPlayers: parseInt(results[3].toFixed(0)),
        boardOwners: results[4],
        boardLifeTiles: results[5]
      };
      return cb(null, contractInfo);
    });
  },
  componentDidMount() {
    var self = this;
    web3 = new Web3(new Web3.providers.HttpProvider('http://rpc.waronether.com'));
//    web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));
    var contractInstance = web3.eth.contract(BoardAbi).at(self.props.contractAddress);
    async.parallel([
      function(cb) {
        contractInstance.getRegisteredContracts(cb);
      },
      function(cb) {
        contractInstance.numPlayers(cb);
      },
      function(cb) {
        contractInstance.boardSize(0, cb);
      },
      function(cb) {
        contractInstance.boardSize(1, cb);
      }
    ], function(err, results) {
      if (err) return self.componentDidMount();
      var registeredContracts = results[0].filter(function(obj) { return obj != '0x0000000000000000000000000000000000000000'; });
      var palette = distinctColors(results[1].toFixed(0));
      self.getContractInfo(contractInstance, registeredContracts, function(err, contractInfo) {
        if (err) return self.componentDidMount();
        self.setState({
          registeredContracts: registeredContracts,
          contractInstance: contractInstance,
          contractInfo: contractInfo,
          rows: results[2].toFixed(0),
          cols: results[3].toFixed(0),
          palette: palette
        });
      });
      contractInstance.Turn({}).watch(function(err, result) {
        if (err) return;
        self.getContractInfo(contractInstance, registeredContracts, function(err, contractInfo) {
          if (err) return;
          self.setState({
            contractInfo: contractInfo
          });
        });
      });
    });
  },
  render() {
    var self = this;
    var rows = [];
    if (self.state.contractInfo) {
      var boardOwners = self.state.contractInfo.boardOwners;
      var boardLifeTiles = self.state.contractInfo.boardLifeTiles;

      for (var j = 0; j < self.state.cols; j++) {
        var cols = [];
        for (var i = 0; i < self.state.rows; i++) {
          var isLifeTile = self.state.registeredContracts.indexOf(boardLifeTiles[i][j]) >= 0;
          var cellContent = self.state.registeredContracts.indexOf(boardOwners[i][j]);
          var tileStyle = {
            backgroundColor: cellContent >= 0 ? self.state.palette[cellContent] : 'white'
          };
          cols.push(
            <td className="boardCell" style={tileStyle} key={j.toString() + 'x' + i.toString()}>
              <div className="boardCellContentWrapper">
                <div className="boardCellContent">
                  {isLifeTile ? '*' : ''}
                </div>
              </div>
            </td>
          );
        }
        rows.push(<tr key={j.toString()}>{cols}</tr>);
      }
      return (
        <div>
          <h5 className="subtitleText">
            Contract address: {self.state.contractInfo.address} <br/><br/>
            Round: {self.state.contractInfo.round + '/' + self.state.contractInfo.numRounds} <br/><br/>
            Players: {self.state.contractInfo.numPlayers} <br/><br/>
            Current player: {self.state.contractInfo.currentPlayer} <br/><br/>
          </h5>
          <div className="boardTableWrapper">
            <table className="boardTable">
              <tbody className="boardTableBody">
                {rows}
              </tbody>
            </table>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          Loading...
        </div>
      );
    }
  }
});

module.exports = Board;
