module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
        bootstrap_material: {
            files: [{
                expand: true,
                cwd: 'node_modules/bootstrap-material-design/dist/css',
                src: '**/*.min.css',
                dest: 'build/css'
            },{
                expand: true,
                cwd: 'node_modules/bootstrap-material-design/dist/js',
                src: '**/*.min.js',
                dest: 'build/js'
            },{
                expand: true,
                cwd: 'node_modules/bootstrap-material-design/bower_components/bootstrap/dist/css',
                src: '**/*.min.css',
                dest: 'build/css'
            },{
                expand: true,
                cwd: 'node_modules/bootstrap-material-design/bower_components/bootstrap/dist/js',
                src: '**/*.min.js',
                dest: 'build/js'
            },{
                expand: true,
                cwd: 'node_modules/bootstrap-material-design/bower_components/jquery/dist',
                src: '**/*.min.js',
                dest: 'build/js'
            }]
        },
        boostrap_social: {
          files: [{
                expand: true,
                cwd: 'bower_components/bootstrap-social',
                src: '*.css',
                dest: 'build/css'
            }]
        },
        html: {
            files: [{
                expand: true,
                cwd: 'src/html',
                src: '**/*.html',
                dest: 'build'
            }]
        },
        img: {
            files: [{
                expand: true,
                cwd: 'img',
                src: '**/*',
                dest: 'build/img'
            }]
        },
        files: {
            files: [{
                expand: true,
                cwd: 'src/files',
                src: '**/*',
                dest: 'build/files'
            },{
              expand: true,
              cwd: '../contract/contract',
              src: '**/*.sol',
              dest: 'build/files'
            }]
        }
    },
    browserify: {
        dev: {
            src: 'src/main.jsx',
            dest: 'tmp/main.js',
            options: {
                debug: true,
                extensions: ['.jsx'],
                transform: [
                    ['reactify', {'es6': true}]
                ]
            }
        }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'tmp/**/*.js',
        dest: 'build/js/site.min.js'
      }
    },
    cssmin: {
        target: {
            files: [{
                expand: true,
                cwd: 'src/css',
                src: ['*.css', '!*.min.css'],
                dest: 'build/css',
                ext: '.min.css'
            }]
        }
    },
    watch: {
        scripts: {
            files: ['src/**/*'],
            tasks: ['default'],
            options: {
                spawn: false
            }
        }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task(s).
  grunt.registerTask('default', ['copy', 'cssmin', 'browserify', 'uglify']);

};
